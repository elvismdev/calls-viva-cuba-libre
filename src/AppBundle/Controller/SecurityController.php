<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        $sesion = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $sesion->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('AppBundle:Security:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }

    public function deniedAction($type = null)
    {
        // Log the user out
        $this->get("request")->getSession()->invalidate();
        $this->get("security.context")->setToken(null);

        if ($type) {
            $notice = '';
            switch ($type) {
                case '1':
                    $notice = 'user_not_school';
                    break;
            }
            $this->get('session')->getFlashBag()->add(
                'notice',
                $notice
            );
        }

        return $this->redirect($this->generateUrl('login'));
    }
}

?>