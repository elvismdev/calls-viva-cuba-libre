<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Payment;
use DateTime;

class PaymentController extends Controller
{
    /**
     * @Route("/newPayment", name="new_payment")
     * @Template()
     */
    public function newPaymentAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($request->getMethod() == 'POST' && $user != 'anon.') {
            $payment = new Payment();
            $payment->setUser($this->get('security.context')->getToken()->getUser());

            $object = array(
                'token' => $request->request->get('stripeToken', ''),
                'card_id' => $request->request->get('cardId', ''),
                'client_ip' => $request->request->get('clientIp', ''),
                'card' => $request->request->get('card', ''),
                'amount' => $request->request->get('amount', '20')
            );

            if (array_search('', $object) !== false) { // At least one value is empty (not allowed)
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Error with some params'
                );

                return $this->redirect($this->generateUrl('homepage'));
            }

            $em = $this->getDoctrine()->getManager();

            $payment->setTokenId($object['token']);
            $payment->setCard($object['card']);
            $payment->setClientIp($object['client_ip']);
            $payment->setCardId($object['card_id']);
            $payment->setAmount($object['amount']);
            $payment->setCreated(new DateTime('now'));

            $em->persist($payment);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Success transaction'
            );
        }

        return $this->redirect($this->generateUrl('homepage'));
    }

}
