<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $tmpl = 'homepage_register.html.twig';
        $user = $this->get('security.context')->getToken()->getUser();
        if ($user != 'anon.')
            $tmpl = 'homepage.html.twig';

        return $this->render("AppBundle:Homepage:$tmpl");
    }
}
