<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\RegistrationType;
use AppBundle\Form\Registration;

class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('AppBundle:User:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function checkPhoneAction($phone)
    {
        $result = $this->get('api')->isPhoneUsed($phone);

        $response = new Response();
        if ($result == 'error')
            $output = array('success' => false, 'result' => 'error');
        else
            $output = array('success' => true, 'result' => $result);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($output));

        return $response;
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $this->setSecurePassword($entity);
            $entity->setRol('ROLE_USER');

            $agent = $em->getRepository('AppBundle:Agent')->find('1');

            $card = $this->get('api')->newCard($agent, $entity);
            $callerID = $this->get('api')->newCaller($agent, $entity);

            $entity->setCardid($card->card->id);
            $entity->setCallerid($callerID->callerId->id);
            $entity->setIdCcCard($callerID->callerId->id_cc_card);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user'));
        }

        return $this->render('AppBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Registrarme ahora'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return $this->render('AppBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Usuario entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:User:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $current_password = $entity->getPassword();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // Changing password if needed
            if ($entity->getPassword() != "") {
                $this->setSecurePassword($entity);
            } else {
                $entity->setPassword($current_password);
            }

            $agent = $em->getRepository('AppBundle:Agent')->find('1');

            $callerID = $this->get('api')->editCallerId($agent, $entity);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('AppBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->saveSecurity($entity, 'user_delete');

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a Usuario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    public function createPasswordAction()
    {
        $form = $this->createFormBuilder()
            ->add('anterior', 'password', array('label' => 'Contraseña Actual'))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field form-control')),
                'required' => true,
                'first_options' => array('label' => 'Nueva Contraseña'),
                'second_options' => array('label' => 'Confirmar Contraseña'),
            ));
        $form = $form->getForm();

        return $this->render('AppBundle:User:password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function changePasswordAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            return $this->redirect($this->generateUrl('user'));
        }

        $form = $this->createFormBuilder()
            ->add('anterior', 'password', array('label' => 'Contraseña Actual'))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field form-control')),
                'required' => true,
                'first_options' => array('label' => 'Nueva Contraseña'),
                'second_options' => array('label' => 'Confirmar Contraseña'),
            ));
        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            // Get the encoder for the users password
            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($user);
            $encoded_pass = $encoder->encodePassword($data['anterior'], $user->getSalt());

            if ($user->getPassword() == $encoded_pass) {
                // Changing password if needed
                $user->setPassword($data['password']);
                $this->setSecurePassword($user);

                $em->flush();

                $type = 'notice';
                $message = 'usuario_change_password_success';

                $this->saveSecurity($user, 'user_change_password');
            } else {
                $type = 'error';
                $message = 'usuario_change_password_bad';
            }
        } else {
            $type = 'error';
            $message = 'usuario_change_password_error';
        }

        $this->get('session')->getFlashBag()->add(
            $type,
            $message
        );

        if ($user->getRol() == 'ROLE_ADMIN')
            return $this->redirect($this->generateUrl('usuario'));
        return $this->redirect($this->generateUrl('inicio'));
    }

    private function setSecurePassword(&$entity)
    {
        $entity->setSalt(md5(time()));
        $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 5000);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }


//    public function registerAction()
//    {
//        $registration = new Registration();
//
//        $form = $this->createForm(new RegistrationType(), $registration, array(
//            'action' => $this->generateUrl('account_create'),
//        ));
//
//        return $this->render(
//            'AppBundle:Account:register.html.twig',
//            array('form' => $form->createView())
//        );
//    }
//
//    public function createAction(Request $request)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $form = $this->createForm(new RegistrationType(), new Registration());
//
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $registration = $form->getData();
//            //var_dump($registration);
//            //exit();
//           $salt = md5(time());
//           $registration->getUser()->setPassword($this->setSecurePassword($registration->getUser(), $salt));
//           $registration->getUser()->setSalt($salt);
//           $registration->getUser()->setIsActive(true);
//
//            $em->persist($registration->getUser());
//            $em->flush();
//
//            return $this->redirect('login');
//        }
//
//        return $this->render(
//            'AppBundle:Account:register.html.twig',
//            array('form' => $form->createView())
//        );
//    }


}