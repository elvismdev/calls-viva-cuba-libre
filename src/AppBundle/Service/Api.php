<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Api
{
    protected $entityManager;
    protected $url;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
        $this->url = 'http://192.168.1.124:5005/api/';
    }

    public function newCard(\AppBundle\Entity\Agent $agent, User $user)
    {
        $post = array(
            'firstname' => $user->getName(),
            'lastname' => $user->getLastname(),
            'country' => $user->getCountry()
            );

        $card = $this->post($agent->getLogin(), $agent->getPasswd(), $this->url, $post);

        $card = json_decode($card);

        if (!isset($card->error)) {
            throw new NotFoundHttpException('Error getting card API. Check your Internet.');
        }

        if ($card->error) {
            throw new NotFoundHttpException('Error creating card. Check parameters. Error: ' . $card->message);
        }

        return $card;
    }

    public function newCaller(\AppBundle\Entity\Agent $agent, User $user)
    {
        $post = array(
            'cid' => $user->getPhone()
        );

        $callerID = $this->postPUT($agent->getLogin(), $agent->getPasswd(), $this->url, $post, 'createCallerId/' . $user->getCardid());

        $callerID = json_decode($callerID);

        if (!isset($callerID->error)) {
            throw new NotFoundHttpException('Error getting callerID API. Check your Internet.');
        }

        if ($callerID->error) {
            throw new NotFoundHttpException('Error creating $callerID. Check parameters. Error: ' . $callerID->message);
        }

        return $callerID;
    }

    public function editCard(\AppBundle\Entity\Agent $agent, User $user)
    {
        $post = array(
            'city' => $user->getCity(),
            'country' => $user->getCountry()
        );

        $card = $this->postPUT($agent->getLogin(), $agent->getPasswd(), $this->url, $post, 'card/' . $user->getCardid());

        $card = json_decode($card);

        if (!isset($card->error)) {
            throw new NotFoundHttpException('Error getting card API. Check your Internet.');
        }

        if ($card->error) {
            throw new NotFoundHttpException('Error creating card. Check parameters. Error: ' . $card->message);
        }

        return $card;
    }

    public function editCallerId(\AppBundle\Entity\Agent $agent, User $user)
    {
        $post = array(
            'cid' => $user->getPhone(),
            'id_cc_card' => $user->getIdCcCard()
        );

        $callerID = $this->postPUT($agent->getLogin(), $agent->getPasswd(), $this->url, $post, 'callerId/' . $user->getCallerid());

        $callerID = json_decode($callerID);

        if (!isset($callerID->error)) {
            throw new NotFoundHttpException('Error getting callerID API. Check your Internet.');
        }

        if ($callerID->error) {
            throw new NotFoundHttpException('Error updating card. Check parameters. Error: ' . $callerID->message);
        }

        return $callerID;
    }

    public function isPhoneUsed($phone)
    {
        $agent = $this->entityManager->getRepository('AppBundle:Agent')->find('1');

        $url = $this->url . 'callerIdCheck/' . $phone;

        $result = $this->httpGet($agent->getLogin(), $agent->getPasswd(), $url);

        $caller = json_decode($result);

        if (!isset($caller->message)) {
            return 'error';
        }

        return ($caller->message == 'CallerId existe') ? true : false;
    }

    private function commandPUT($user, $passwd, $post = null, $url)
    {
        if ($post)
            $post = "-d '$post'";

        $output = '';
        exec("curl --user $user:$passwd -X PUT $post $url", $output);

        return $output;
    }

    private function post($user, $passwd, $url, $fields, $type='card')
    {
        $post_field_string = http_build_query($fields, '', '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url . $type);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $passwd);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    private function postPUT($user, $passwd, $url, $fields, $type='card')
    {
        $post_field_string = http_build_query($fields, '', '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url . $type);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $passwd);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    private function httpGet($user, $passwd, $url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $passwd);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

}